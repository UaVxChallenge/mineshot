package com.src.csgo.constructers;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

import com.src.csgo.GunType;
import com.src.csgo.Guns;

public class Shoot implements Listener {

	public Player p = null;
	public int i = 0;
	public int iTotal = 0;
	public HashMap<Guns, Integer> keepAmmo = new HashMap<Guns, Integer>();
	public HashMap<Guns, Integer> keepMaxAmmo = new HashMap<Guns, Integer>();
	public HashMap<Guns, Boolean> canShoot = new HashMap<Guns, Boolean>();

	public Shoot(Player player) {

		p = player;

	}

	public void onPlayerShoot(Guns guns, GunType gunType) {

		if (canShoot.get(guns) == false) {
			return;
		}

		if (keepAmmo.containsKey(guns)) {
			i = keepAmmo.get(guns);
		}

		if (keepMaxAmmo.containsKey(guns)) {
			iTotal = keepMaxAmmo.get(guns);
		}

		if (i > 0) {

			Snowball s = p.launchProjectile(Snowball.class);
			Vector velocity = s.getVelocity();
			double speed = velocity.length();
			Vector direction = new Vector(velocity.getX() / speed,
					velocity.getY() / speed, velocity.getZ() / speed);

			if (gunType == GunType.AssualtRifle) {
				s.setVelocity(new Vector(direction.getX()
						+ (Math.random() - 0.5) / 12, direction.getY()
						+ (Math.random() - 0.5) / 12, direction.getZ()
						+ (Math.random() - 0.5) / 12).normalize().multiply(6));
			}
			if (gunType == GunType.SniperRifle) {
				s.setVelocity(new Vector(direction.getX()
						+ (Math.random() - 0.5) / 15, direction.getY()
						+ (Math.random() - 0.5) / 15, direction.getZ()
						+ (Math.random() - 0.5) / 15).normalize().multiply(6));

			}

			ItemStack gun = new ItemStack(p.getItemInHand().getType(), 1,
					(short) (iTotal / (i + 0.5)));
			ItemMeta gunM = gun.getItemMeta();

			i--;

			keepAmmo.put(guns, i);

			gunM.setDisplayName(ChatColor.BOLD + "<< " + i + " / " + iTotal
					+ " >>");
			gun.setItemMeta(gunM);

			p.setItemInHand(gun);
		}

	}

	public void setAmmo(int ammo) {

		i = ammo;
		iTotal = ammo;
	}

	public void setCanShoot(Guns gun) {
		canShoot.put(gun, true);
	}

	public void setMaxAmmo(int i, Guns gun) {
		keepMaxAmmo.put(gun, i);
	}

	public void reloadGun(Guns gun) {

		keepAmmo.put(gun, keepMaxAmmo.get(gun));

		canShoot.put(gun, true);

	}

	public Player getPlayer() {
		return p;
	}

	public void setCantShoot(Guns gun) {
		canShoot.put(gun, false);
	}

}
