package com.src.csgo.scoreboards;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import com.src.csgo.arena.GameStart;

public class PreGame {

	public static Scoreboard pregame;
	private static Objective PreGame;
	private static Score playersName;
	private static Score players;
	private static Score gamename;
	private static Score gameTime;

	private static int animate = 1;

	public static void makeScoreboard(Player p) {

		int i = Bukkit.getOnlinePlayers().size();

		pregame = Bukkit.getScoreboardManager().getNewScoreboard();
		PreGame = pregame.registerNewObjective("CSGO", "CSGO");

		PreGame.setDisplaySlot(DisplaySlot.SIDEBAR);
		PreGame.setDisplayName(ChatColor.BOLD + "" + ChatColor.YELLOW + "M"
				+ ChatColor.WHITE + "ine Shot");

		Score blank1 = PreGame.getScore(ChatColor.YELLOW + " ");
		playersName = PreGame.getScore(ChatColor.BLUE + "Players in game");
		players = PreGame.getScore(ChatColor.WHITE + "" + i + "");
		Score blank2 = PreGame.getScore(ChatColor.RED + " ");
		gamename = PreGame.getScore(ChatColor.BLUE + "Time until start");
		gameTime = PreGame.getScore(ChatColor.WHITE + "" + GameStart.j);
		Score blank3 = PreGame.getScore(ChatColor.BLUE + " ");

		blank1.setScore(15);
		playersName.setScore(14);
		players.setScore(13);
		blank2.setScore(12);
		gamename.setScore(11);
		gameTime.setScore(10);
		blank3.setScore(9);

		p.setScoreboard(pregame);

	}

	public static void updateScoreboard(Player p) {

		int i = Bukkit.getOnlinePlayers().size();

		pregame = Bukkit.getScoreboardManager().getNewScoreboard();
		PreGame = pregame.registerNewObjective("CSGO", "CSGO");

		PreGame.setDisplaySlot(DisplaySlot.SIDEBAR);

		if (animate == 0) {
			PreGame.setDisplayName(ChatColor.BOLD + "" + ChatColor.YELLOW + "M"
					+ ChatColor.WHITE + "ine Shot");
		} else if (animate == 1) {
			PreGame.setDisplayName(ChatColor.BOLD + "" + ChatColor.YELLOW
					+ "Mi" + ChatColor.WHITE + "ne Shot");
		} else if (animate == 2) {
			PreGame.setDisplayName(ChatColor.BOLD + "" + ChatColor.YELLOW
					+ "Min" + ChatColor.WHITE + "e Shot");
		} else if (animate == 3) {
			PreGame.setDisplayName(ChatColor.BOLD + "" + ChatColor.YELLOW
					+ "Mine" + ChatColor.WHITE + " Shot");
		} else if (animate == 4) {
			PreGame.setDisplayName(ChatColor.BOLD + "" + ChatColor.YELLOW
					+ "Mine S" + ChatColor.WHITE + "hot");
		} else if (animate == 5) {
			PreGame.setDisplayName(ChatColor.BOLD + "" + ChatColor.YELLOW
					+ "Mine Sh" + ChatColor.WHITE + "ot");
		} else if (animate == 6) {
			PreGame.setDisplayName(ChatColor.BOLD + "" + ChatColor.YELLOW
					+ "Mine Sho" + ChatColor.WHITE + "t");
		} else if (animate == 7) {
			PreGame.setDisplayName(ChatColor.BOLD + "" + ChatColor.YELLOW
					+ "Mine Shot");
		} else if (animate == 8) {
			PreGame.setDisplayName(ChatColor.BOLD + "" + ChatColor.YELLOW
					+ "Mine Shot");
		} else if (animate == 9) {
			PreGame.setDisplayName(ChatColor.BOLD + "" + ChatColor.WHITE
					+ "Mine Shot");
		} else if (animate == 10) {
			PreGame.setDisplayName(ChatColor.BOLD + "" + ChatColor.YELLOW
					+ "Mine Shot");
			animate = 0;
		}

		Score blank1 = PreGame.getScore(ChatColor.YELLOW + " ");
		playersName = PreGame.getScore(ChatColor.BLUE + "Players in game");
		players = PreGame.getScore(ChatColor.WHITE + "" + i + "");
		Score blank2 = PreGame.getScore(ChatColor.RED + " ");
		gamename = PreGame.getScore(ChatColor.BLUE + "Time until start");
		gameTime = PreGame.getScore(ChatColor.WHITE + "" + GameStart.j);
		Score blank3 = PreGame.getScore(ChatColor.BLUE + " ");

		blank1.setScore(15);
		playersName.setScore(14);
		players.setScore(13);
		blank2.setScore(12);
		gamename.setScore(11);
		gameTime.setScore(10);
		blank3.setScore(9);

		p.setScoreboard(pregame);

		animate++;
	}

}
