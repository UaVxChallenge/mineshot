package com.src.csgo.scoreboards;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import com.src.csgo.arena.Team;

public class InGameScore {

	public static Scoreboard ingame;
	private static Objective InGame;
	private static Score playersName;
	private static Score players;
	private static Score gamename;
	private static Score gameTime;

	public static void makeScoreboard(Player p) {

		ingame = Bukkit.getScoreboardManager().getNewScoreboard();
		InGame = ingame.registerNewObjective("CSGO", "CSGO");

		InGame.setDisplaySlot(DisplaySlot.SIDEBAR);
		InGame.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD
				+ "Mine Shot");

		Score blank1 = InGame.getScore(ChatColor.GOLD + " ");
		playersName = InGame.getScore(ChatColor.RED + "Red Lives");
		players = InGame.getScore(ChatColor.WHITE + " 25");
		Score blank2 = InGame.getScore(ChatColor.RED + " ");
		gamename = InGame.getScore(ChatColor.BLUE + "Blue Lives");
		gameTime = InGame.getScore(ChatColor.WHITE + " 25");
		Score blank3 = InGame.getScore(ChatColor.BLUE + " ");

		blank1.setScore(6);
		playersName.setScore(5);
		players.setScore(4);
		blank2.setScore(3);
		gamename.setScore(2);
		gameTime.setScore(1);
		blank3.setScore(0);

		p.setScoreboard(ingame);

	}

	public static void updateScoreboard(Player p) {

		ingame = Bukkit.getScoreboardManager().getNewScoreboard();
		InGame = ingame.registerNewObjective("CSGO", "CSGO");

		InGame.setDisplaySlot(DisplaySlot.SIDEBAR);

		InGame.setDisplayName(ChatColor.YELLOW + "" + ChatColor.BOLD
				+ "Mine Shot");

		Score blank1 = InGame.getScore(ChatColor.GOLD + " ");
		playersName = InGame.getScore(ChatColor.RED + "Red Lives");
		players = InGame.getScore(ChatColor.WHITE + " "
				+ Team.getLives(Team.getTeam("Red")));
		Score blank2 = InGame.getScore(ChatColor.RED + " ");
		gamename = InGame.getScore(ChatColor.BLUE + "Blue Lives");
		gameTime = InGame.getScore(ChatColor.WHITE + " "
				+ Team.getLives(Team.getTeam("Blue")));
		Score blank3 = InGame.getScore(ChatColor.BLUE + " ");

		blank1.setScore(6);
		playersName.setScore(5);
		players.setScore(4);
		blank2.setScore(3);
		gamename.setScore(2);
		gameTime.setScore(1);
		blank3.setScore(0);

		p.setScoreboard(ingame);
	}
}
