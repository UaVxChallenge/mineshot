package com.src.csgo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.src.csgo.arena.GameStages;
import com.src.csgo.arena.GameStart;
import com.src.csgo.listeners.EntityDamage;
import com.src.csgo.listeners.Gui;
import com.src.csgo.listeners.Pickupitem;
import com.src.csgo.listeners.ReloadEvent;
import com.src.csgo.listeners.ShootEvent;
import com.src.csgo.locationUtils.LocationUtils;
import com.src.csgo.texturelistener.PacketInjector;

public class Main extends JavaPlugin {

	public static Connection connection;
	public static Connection connectionGuns;
	public static HashMap<Player, Integer> coins = new HashMap<Player, Integer>();
	public static PacketInjector injector;
	public static String bukkitVersion;

	public static Plugin instance;

	@Override
	public void onEnable() {

		bukkitVersion = Bukkit.getServer().getClass().getPackage().getName()
				.split("\\.")[3];

		injector = new PacketInjector();

		instance = this;

		getServer().getMessenger().registerOutgoingPluginChannel(this,
				"BungeeCord");

		if (getConfig() != null) {
			LocationUtils.setupSpawns();
		}

		Bukkit.getServer().getPluginManager()
				.registerEvents(new ShootEvent(), this);
		Bukkit.getServer().getPluginManager()
				.registerEvents(new ReloadEvent(), this);
		Bukkit.getServer().getPluginManager()
				.registerEvents(new EntityDamage(), this);
		Bukkit.getServer().getPluginManager()
				.registerEvents(new GameStart(), this);
		Bukkit.getServer().getPluginManager()
				.registerEvents(new Pickupitem(), this);
		Bukkit.getServer().getPluginManager().registerEvents(new Gui(), this);

		GameStages.setState(GameStages.LOBBY);
		GameStart.doPregameSchedule();

		doTime();
	}

	@Override
	public void onDisable() {

		try {
			if (connection != null && !(connection.isClosed())) {
				connection.close();
			}
			if (connectionGuns != null && !(connectionGuns.isClosed())) {
				connectionGuns.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public synchronized static void openConnection() {
		try {
			connection = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/Minigame", "root", "Ne@t-815");

			Bukkit.getLogger().info("Connection opened");
		} catch (Exception ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public synchronized static void closeConnection() {
		try {
			if (connection != null) {
				connection.close();
				Bukkit.getLogger().info("Connection closed");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public synchronized static boolean playerDataContainsPlayer(Player player) {

		try {
			openConnection();
			PreparedStatement sql = connection
					.prepareStatement("SELECT * FROM `player_data` WHERE uuid=?;");
			sql.setString(1, "" + player.getUniqueId());
			ResultSet resultset = sql.executeQuery();
			boolean containsPlayer = resultset.next();

			sql.close();
			resultset.close();

			return containsPlayer;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			closeConnection();
		}

	}

	public synchronized static void openGunsConnection() {
		try {
			connectionGuns = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/BombingRun", "root",
					"Ne@t-815");

			Bukkit.getLogger().info("Connection Bombing Run opened");
		} catch (Exception ex) {
			throw new ExceptionInInitializerError(ex);
		}
	}

	public synchronized static void closeGunsConnection() {
		try {
			if (connectionGuns != null) {
				connectionGuns.close();
				Bukkit.getLogger().info("Connection closed");
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public synchronized static boolean gunsContainsPlayer(Player player) {
		if (connectionGuns == null) {
			openGunsConnection();
		}
		try {
			PreparedStatement sql = connectionGuns
					.prepareStatement("SELECT * FROM `unlockedGuns` WHERE uuid=?;");
			sql.setString(1, "" + player.getUniqueId());
			ResultSet resultset = sql.executeQuery();
			boolean containsPlayer = resultset.next();

			sql.close();
			resultset.close();

			return containsPlayer;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	public static void getCoins(Player p) {
		try {
			if (playerDataContainsPlayer(p)) {

				openConnection();

				PreparedStatement sql = Main.connection
						.prepareStatement("SELECT coins FROM `player_data` WHERE uuid=?;");
				sql.setString(1, "" + p.getUniqueId());

				ResultSet result = sql.executeQuery();
				result.next();

				coins.put(p, result.getInt("coins"));

				sql.close();
				result.close();

			} else {

				coins.put(p, 0);

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			closeConnection();
		}

	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {

		if (!(sender instanceof Player)) {
			return false;
		}
		Player p = (Player) sender;

		if (cmd.getName().equalsIgnoreCase("Hub")) {
			ByteArrayDataOutput out = ByteStreams.newDataOutput();

			out.writeUTF("Connect");
			out.writeUTF("Hub");

			p.sendPluginMessage(Main.instance, "BungeeCord", out.toByteArray());
			return true;
		}
		if (cmd.getName().equalsIgnoreCase("Leave")) {
			ByteArrayDataOutput out = ByteStreams.newDataOutput();

			out.writeUTF("Connect");
			out.writeUTF("Hub");

			p.sendPluginMessage(Main.instance, "BungeeCord", out.toByteArray());
			return true;
		}

		if (cmd.getName().equalsIgnoreCase("SetSpawn")) {
			if (p.hasPermission("Mineshot.Owner")) {
				if (args.length == 0) {
					return false;
				}
				if (args.length >= 1) {

					if (args[0].equalsIgnoreCase("Red")) {
						getConfig().set("Red.x", p.getLocation().getX());
						getConfig().set("Red.y", p.getLocation().getY());
						getConfig().set("Red.z", p.getLocation().getZ());
						saveConfig();
					}
					if (args[0].equalsIgnoreCase("Blue")) {
						getConfig().set("Blue.x", p.getLocation().getX());
						getConfig().set("Blue.y", p.getLocation().getY());
						getConfig().set("Blue.z", p.getLocation().getZ());
						saveConfig();
					}
					if (args[0].equalsIgnoreCase("Lobby")) {
						getConfig().set("Lobby.x", p.getLocation().getX());
						getConfig().set("Lobby.y", p.getLocation().getY());
						getConfig().set("Lobby.z", p.getLocation().getZ());
						saveConfig();
					}
				}

			}
		}
		return false;
	}

	public void doTime() {
		Bukkit.getServer().getScheduler()
				.scheduleSyncRepeatingTask(instance, new Runnable() {

					@Override
					public void run() {

						Bukkit.getWorld("World").setTime(20000l);
						Bukkit.getWorld("World").setStorm(false);

					}
				}, 20, 20);
	}

	public static ItemStack addGlow(ItemStack item) {
		net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack
				.asNMSCopy(item);
		NBTTagCompound tag = null;
		if (!nmsStack.hasTag()) {
			tag = new NBTTagCompound();
			nmsStack.setTag(tag);
		}
		if (tag == null)
			tag = nmsStack.getTag();
		NBTTagList ench = new NBTTagList();
		tag.set("ench", ench);
		nmsStack.setTag(tag);
		return CraftItemStack.asCraftMirror(nmsStack);
	}
}
