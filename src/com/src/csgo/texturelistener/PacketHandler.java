package com.src.csgo.texturelistener;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPromise;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.src.csgo.Main;

public class PacketHandler extends ChannelDuplexHandler {
	private Player p;

	public PacketHandler(final Player p) {
		this.p = p;
	}

	@Override
	public void write(ChannelHandlerContext ctx, Object msg,
			ChannelPromise promise) throws Exception {
		super.write(ctx, msg, promise);
	}

	@Override
	public void channelRead(ChannelHandlerContext c, Object m) throws Exception {
		if (m.getClass().getSimpleName()
				.equalsIgnoreCase("PacketPlayInResourcePackStatus")) {
			String s = Reflection.getFieldValue(m, "b").toString();
			if (s.equals("DECLINED")) {

				this.p.sendMessage(ChatColor.RED
						+ "You must download the texture pack! Sending you to the Hub.");

				ByteArrayDataOutput out = ByteStreams.newDataOutput();

				out.writeUTF("Connect");
				out.writeUTF("Hub");

				p.sendPluginMessage(Main.instance, "BungeeCord",
						out.toByteArray());

			}
			if (s.equals("FAILED_DOWNLOAD")) {

				this.p.sendMessage(ChatColor.RED
						+ "Texture pack failed to download. Sending you to the Hub!");

				ByteArrayDataOutput out = ByteStreams.newDataOutput();

				out.writeUTF("Connect");
				out.writeUTF("Hub");

				p.sendPluginMessage(Main.instance, "BungeeCord",
						out.toByteArray());

			}
			if (s.equals("ACCEPTED")) {
			}
			if (s.equals("SUCCESSFULLY_LOADED")) {

				doMessage(this.p);

				this.p.sendMessage("Texture pack installed! Thanks for playing!");
				return;
			}
		} else {
			super.channelRead(c, m);
		}
	}

	@SuppressWarnings("rawtypes")
	public void doMessage(Player player) {

		try {
			String version = Bukkit.getServer().getClass().getPackage()
					.getName().replace(".", ",").split(",")[3];
			Object nmsPlayer = player.getClass().getMethod("getHandle")
					.invoke(player);
			Object connection = nmsPlayer.getClass()
					.getField("playerConnection").get(nmsPlayer);
			Class<?> chatSerializer = Class.forName("net.minecraft.server."
					+ version + ".IChatBaseComponent$ChatSerializer");
			Class<?> chatComponent = Class.forName("net.minecraft.server."
					+ version + ".IChatBaseComponent");
			Class<?> packet = Class.forName("net.minecraft.server." + version
					+ ".PacketPlayOutChat");
			Constructor constructor = packet.getConstructor(chatComponent);

			Object text = chatSerializer
					.getMethod("a", String.class)
					.invoke(chatSerializer,
							"{text: \"Click this text for the maker of the texture pack! <3\", color: \"red\", bold: \"false\", clickEvent: {\"action\": \"open_url\" , value: \"https://www.youtube.com/c/MikeVResourcePacksyPvP\"}, hoverEvent: {\"action\": \"show_text\", value:{text: \"Click Link\"}}}");
			Object packetFinal = constructor.newInstance(text);

			Field field = packetFinal.getClass().getDeclaredField("a");
			field.setAccessible(true);
			field.set(packetFinal, text);
			connection
					.getClass()
					.getMethod(
							"sendPacket",
							Class.forName("net.minecraft.server." + version
									+ ".Packet"))
					.invoke(connection, packetFinal);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
