package com.src.csgo.locationUtils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.src.csgo.Main;

public class LocationUtils {

	public static Location RedSpawn = null;
	public static Location BlueSpawn = null;
	public static Location Lobby = null;

	public static void setupSpawns() {

		Location RedTemp = new Location(Bukkit.getWorld("World"), Main.instance
				.getConfig().getDouble("Red.x"), Main.instance.getConfig()
				.getDouble("Red.y"), Main.instance.getConfig().getDouble(
				"Red.z"));

		RedSpawn = RedTemp;

		Location BlueTemp = new Location(Bukkit.getWorld("World"),
				Main.instance.getConfig().getDouble("Blue.x"), Main.instance
						.getConfig().getDouble("Blue.y"), Main.instance
						.getConfig().getDouble("Blue.z"));

		BlueSpawn = BlueTemp;

		Location LobbyTemp = new Location(Bukkit.getWorld("World"),
				Main.instance.getConfig().getDouble("Lobby.x"), Main.instance
						.getConfig().getDouble("Lobby.y"), Main.instance
						.getConfig().getDouble("Lobby.z"));

		Lobby = LobbyTemp;
	}

}
