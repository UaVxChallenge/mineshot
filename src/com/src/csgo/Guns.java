package com.src.csgo;

import org.bukkit.Material;

public enum Guns {

	G36C(GunType.AssualtRifle, 4.0, 25, Material.WOOD_PICKAXE), M4(
			GunType.AssualtRifle, 3.0, 25, Material.GOLD_PICKAXE), Scar(
			GunType.AssualtRifle, 5.0, 20, Material.DIAMOND_PICKAXE), Barrett(
			GunType.SniperRifle, 15.0, 5, Material.DIAMOND_AXE);

	private GunType gunT;
	private Double Power;
	private Integer Ammo;
	private Material MaterialT;

	private Guns(GunType gunType, Double power, Integer ammo, Material material) {

		gunT = gunType;
		Power = power;
		Ammo = ammo;
		MaterialT = material;

	}

	public GunType getGunType() {
		return gunT;
	}

	public Double getPower() {
		return Power;
	}

	public Integer getAmmo() {
		return Ammo;
	}

	public Material getMaterial() {
		return MaterialT;
	}

	public static Guns[] getGuns() {
		Guns[] guns = { G36C, M4, Scar, Barrett };
		return guns;
	}

}
