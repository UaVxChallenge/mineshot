package com.src.csgo.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.src.csgo.Guns;
import com.src.csgo.arena.GameStart;

public class Gui implements Listener {

	@EventHandler
	public void onPlayerClick(PlayerInteractEvent e) {

		if (e.getAction() == Action.RIGHT_CLICK_AIR
				|| e.getAction() == Action.RIGHT_CLICK_BLOCK) {

			if (e.getItem().getType() == Material.COOKED_BEEF) {

				openWeaponSelector(e.getPlayer());

			}

		}

	}

	public void openWeaponSelector(Player p) {

		if (GameStart.unlockedGuns == null
				|| !(GameStart.unlockedGuns.containsKey(p))) {
			GameStart.getGuns(p);
		}

		Inventory Weapons = Bukkit.createInventory(null, 9, ChatColor.GREEN
				+ "" + ChatColor.BOLD + "[Weapon Selector]");

		ItemStack M4 = new ItemStack(Guns.M4.getMaterial());
		ItemMeta MM = M4.getItemMeta();

		MM.setDisplayName(ChatColor.GREEN + "M4 (Unlocked)");
		M4.setItemMeta(MM);

		ItemStack G36C = new ItemStack(Guns.G36C.getMaterial());
		ItemMeta Gm = G36C.getItemMeta();

		Gm.setDisplayName(ChatColor.RED + "G36C (Locked)");

		ItemStack Scar = new ItemStack(Guns.Scar.getMaterial());
		ItemMeta Sm = Scar.getItemMeta();

		Sm.setDisplayName(ChatColor.RED + "Scar-H (Locked)");

		ItemStack Bar = new ItemStack(Guns.Barrett.getMaterial());
		ItemMeta Bm = Bar.getItemMeta();

		Bm.setDisplayName(ChatColor.RED + "Barrett 50Cal (Locked)");

		for (Guns g : GameStart.unlockedGuns.get(p)) {

			if (g == Guns.G36C) {
				Gm.setDisplayName(ChatColor.GREEN + "G36C (Unlocked)");
			} else if (g == Guns.Scar) {
				Sm.setDisplayName(ChatColor.GREEN + "Scar-H (Unlocked)");
			} else if (g == Guns.Barrett) {
				Bm.setDisplayName(ChatColor.GREEN + "Barrett 50Cal (Unlocked)");
			}

		}

		G36C.setItemMeta(Gm);
		Scar.setItemMeta(Sm);
		Bar.setItemMeta(Bm);

		ItemStack blank = new ItemStack(Material.STAINED_GLASS_PANE, 1,
				(short) 14);
		ItemMeta blankm = blank.getItemMeta();

		blankm.setDisplayName("");
		blank.setItemMeta(blankm);

		Weapons.setItem(0, blank);
		Weapons.setItem(1, M4);
		Weapons.setItem(2, G36C);
		Weapons.setItem(3, Scar);
		Weapons.setItem(4, blank);
		Weapons.setItem(5, blank);
		Weapons.setItem(6, Bar);
		Weapons.setItem(7, blank);
		Weapons.setItem(8, blank);

		p.openInventory(Weapons);

	}

	@EventHandler
	public void onICE(InventoryClickEvent e) {

		Player p = (Player) e.getWhoClicked();

		if (e.getInventory()
				.getName()
				.equals(ChatColor.GREEN + "" + ChatColor.BOLD
						+ "[Weapon Selector]")) {

			if (!(GameStart.unlockedGuns.containsKey(p))
					|| GameStart.unlockedGuns == null) {
				GameStart.getGuns(p);
			}
			e.setCancelled(true);

			switch (e.getSlot()) {
			case 1:
				if (GameStart.unlockedGuns.get(p).contains(Guns.M4)) {
					GameStart.playerGun.put(p, Guns.M4);
				} else {
					p.sendMessage(ChatColor.RED
							+ "You have not unlocked this gun!");
				}
				p.closeInventory();
				break;
			case 2:
				if (GameStart.unlockedGuns.get(p).contains(Guns.G36C)) {
					GameStart.playerGun.put(p, Guns.G36C);
				} else {
					p.sendMessage(ChatColor.RED
							+ "You have not unlocked this gun!");
				}
				p.closeInventory();
				break;
			case 3:
				if (GameStart.unlockedGuns.get(p).contains(Guns.Scar)) {
					GameStart.playerGun.put(p, Guns.Scar);
				} else {
					p.sendMessage(ChatColor.RED
							+ "You have not unlocked this gun!");
				}
				p.closeInventory();
				break;
			case 6:
				if (GameStart.unlockedGuns.get(p).contains(Guns.Barrett)) {
					GameStart.playerGun.put(p, Guns.Barrett);
				} else {
					p.sendMessage(ChatColor.RED
							+ "You have not unlocked this gun!");
				}
				p.closeInventory();
				break;
			default:
				break;

			}

		}

	}

}
