package com.src.csgo.listeners;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.src.csgo.Guns;
import com.src.csgo.Main;
import com.src.csgo.arena.GameStages;
import com.src.csgo.constructers.Shoot;

public class ReloadEvent implements Listener {

	private static HashMap<Player, Integer> Reload = new HashMap<Player, Integer>();

	@EventHandler
	public void ReloadGun(PlayerInteractEvent e) {

		if (GameStages.isState(GameStages.INGAME)) {

			for (Guns g : Guns.getGuns()) {
				if ((e.getPlayer().getItemInHand().getType().equals(g
						.getMaterial()))) {

					if (e.getAction() == Action.LEFT_CLICK_AIR
							|| e.getAction() == Action.LEFT_CLICK_BLOCK) {
						Shoot s = null;
						for (Shoot sh : ShootEvent.playerShoot) {

							if (sh.getPlayer() == e.getPlayer()) {
								s = sh;
							}

						}

						if (Reload.containsKey(e.getPlayer())) {
							return;
						}

						final Shoot r = s;
						if (r == null) {
							return;
						}

						final Player p = e.getPlayer();
						if (Reload == null || !(Reload.containsKey(p))) {

							for (Guns a : Guns.getGuns()) {

								if (a.getMaterial() != e.getPlayer()
										.getItemInHand().getType()) {

								} else if (a.getMaterial() == e.getPlayer()
										.getItemInHand().getType()) {

									g = a;

									r.setCantShoot(a);
								}

							}
							final Guns gFinal = g;
							int id = Bukkit.getScheduler()
									.scheduleSyncRepeatingTask(Main.instance,
											new Runnable() {

												int time = 5;

												@Override
												public void run() {

													p.playSound(
															p.getLocation(),
															Sound.ORB_PICKUP,
															20f, 20f);

													Float f = (float) time
															/ (float) 5;
													p.setExp(f);
													if (time == 0) {
														r.reloadGun(gFinal);
														cancelReloadTime(p);
														p.setExp(1.0f);
													}
													time--;
												}
											}, 20, 20);
							Reload.put(p, id);
						}
					}
				}
			}
		}
	}

	private void cancelReloadTime(Player p) {

		Bukkit.getScheduler().cancelTask(Reload.get(p));
		Reload.remove(p);

	}

}
