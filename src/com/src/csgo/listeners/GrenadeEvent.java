package com.src.csgo.listeners;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

import com.src.csgo.Main;
import com.src.csgo.arena.GameStages;

public class GrenadeEvent implements Listener {
	public static HashMap<Integer, List<Block>> respawnBlocks = new HashMap<Integer, List<Block>>();

	@EventHandler
	public void Grenades(PlayerInteractEvent e) {

		if (GameStages.isState(GameStages.INGAME)) {

			if (e.getPlayer().getItemInHand().getType().equals(Material.ARROW)) {

				final Arrow a = e.getPlayer().launchProjectile(Arrow.class);
				Vector velocity = a.getVelocity();
				double speed = velocity.length();
				Vector direction = new Vector(velocity.getX() / speed,
						velocity.getY() / speed, velocity.getZ() / speed);

				a.setVelocity(new Vector(direction.getX()
						+ (Math.random() - 0.5) / 100, direction.getY()
						+ (Math.random() - 0.5) / 100, direction.getZ()
						+ (Math.random() - 0.5) / 100).normalize().multiply(6));

				a.setBounce(true);

				Bukkit.getServer().getScheduler()
						.scheduleSyncDelayedTask(Main.instance, new Runnable() {

							@Override
							public void run() {

								a.getWorld().createExplosion(a.getLocation(),
										5.0f);

							}
						}, 40);

			}

		}
	}

	@EventHandler
	public void onExplode(EntityExplodeEvent e) {
		final HashMap<Location, Material> blocks = new HashMap<>();
		final Entity entity = e.getEntity();
		for (Block b : e.blockList()) {
			blocks.put(b.getLocation(), b.getType());
		}
		Bukkit.getScheduler().scheduleSyncDelayedTask(Main.instance,
				new Runnable() {
					@Override
					public void run() {
						for (Entity ent : entity.getWorld().getNearbyEntities(
								entity.getLocation(), 10, 10, 10)) {
							if (ent instanceof Item) {
								ent.remove();
							}
						}
					}
				}, 1);
		Bukkit.getScheduler().scheduleSyncDelayedTask(Main.instance,
				new Runnable() {
					@Override
					public void run() {
						blockRegen(blocks);
					}
				}, 20);
	}

	public void blockRegen(final HashMap<Location, Material> blocks) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(Main.instance,
				new Runnable() {
					@Override
					public void run() {
						if (!blocks.isEmpty()) {
							HashMap<Double, Location> hashList = new HashMap<>();
							for (Location loc : blocks.keySet()) {
								hashList.put(loc.getY(), loc);
							}
							List<Double> doubles = new ArrayList<>();
							for (double d : hashList.keySet()) {
								doubles.add(d);
							}
							Location loc = hashList.get(doubles
									.get(minIndex(doubles)));
							Material b = blocks.get(loc);
							loc.getWorld().getBlockAt(loc).setType(b);
							for (Entity ent : loc.getWorld().getNearbyEntities(
									loc, 15D, 15D, 15D)) {
								if (ent instanceof Player) {
									((Player) ent).playSound(loc,
											Sound.CHICKEN_EGG_POP, 5F, 5F);
								}
							}
							blocks.remove(loc);
							blockRegen(blocks);
						}
					}
				}, 5);
	}

	public static int minIndex(List<Double> list) {
		return list.indexOf(Collections.min(list));
	}

}
