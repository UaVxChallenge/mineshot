package com.src.csgo.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class Pickupitem implements Listener {

	@EventHandler
	public void onPlayerdropItem(PlayerDropItemEvent e) {

		e.setCancelled(true);

	}

	@EventHandler
	public void onPlayerPickUpItem(PlayerPickupItemEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void BreakBlock(BlockBreakEvent e) {
		e.setCancelled(true);
	}
}
