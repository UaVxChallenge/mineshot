package com.src.csgo.listeners;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.src.csgo.GunType;
import com.src.csgo.Guns;
import com.src.csgo.Main;
import com.src.csgo.arena.GameStages;
import com.src.csgo.constructers.Shoot;

public class ShootEvent implements Listener {

	public static List<Shoot> playerShoot = new ArrayList<Shoot>();
	public static ArrayList<Player> cooldown = new ArrayList<Player>();

	@EventHandler
	public void onPlayerShoot(PlayerInteractEvent e) {

		if (GameStages.isState(GameStages.INGAME)) {

			if (e.getAction() != Action.RIGHT_CLICK_AIR) {
				return;
			}
			Shoot s = null;

			for (Shoot sh : playerShoot) {

				if (sh.getPlayer() == e.getPlayer()) {
					s = sh;
				}
			}

			if (s == null) {
				Shoot rs = new Shoot(e.getPlayer());
				s = rs;
				playerShoot.add(s);
			}

			final Player p = e.getPlayer();
			final Shoot r = s;
			for (Guns a : Guns.getGuns()) {

				if (a.getMaterial() == p.getItemInHand().getType()) {
					if (!(r.keepAmmo.containsKey(p))) {
						SetShooter(p);
					}
					if (a.getGunType() == GunType.SniperRifle) {
						s.onPlayerShoot(a, GunType.SniperRifle);
						s.setCantShoot(a);

						final Guns g = a;

						Bukkit.getScheduler().scheduleSyncDelayedTask(
								Main.instance, new Runnable() {

									@Override
									public void run() {
										r.setCanShoot(g);
									}
								}, 20 * 5);
						return;
					} else if (a.getGunType() == GunType.AssualtRifle) {
						r.onPlayerShoot(a, GunType.AssualtRifle);
						return;
					}
				}
			}
		}
	}

	public static void SetShooter(Player p) {

		if (GameStages.isState(GameStages.INGAME)) {

			Shoot s = null;
			for (Shoot sh : playerShoot) {

				if (sh.getPlayer() == p) {
					s = sh;
				}

			}
			if (s == null) {
				s = new Shoot(p);
			}
			for (Guns a : Guns.getGuns()) {

				if (a.getMaterial() == p.getItemInHand().getType()) {
					s.setMaxAmmo(a.getAmmo(), a);
					s.setAmmo(a.getAmmo());
					if (s.canShoot.get(a) == null || s.canShoot == null) {
						s.setCanShoot(a);
					}
				}
			}

			playerShoot.add(s);

			ItemStack gun = new ItemStack(p.getItemInHand().getType());
			ItemMeta gunM = gun.getItemMeta();

			for (Guns a : Guns.getGuns()) {

				if (a.getMaterial() == p.getItemInHand().getType()) {
					gunM.setDisplayName(ChatColor.BOLD + "<< "
							+ s.keepAmmo.get(a) + " / " + s.keepMaxAmmo.get(a)
							+ " >>");
				}
			}
			gun.setItemMeta(gunM);

			p.setItemInHand(gun);

			return;

		}
	}
}
