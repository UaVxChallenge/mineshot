package com.src.csgo.listeners;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.src.csgo.Guns;
import com.src.csgo.Main;
import com.src.csgo.arena.GameStages;
import com.src.csgo.arena.GameStart;
import com.src.csgo.arena.Team;
import com.src.csgo.constructers.Shoot;
import com.src.csgo.locationUtils.LocationUtils;

public class EntityDamage implements Listener {

	public static ArrayList<Player> spawnProt = new ArrayList<Player>();

	@EventHandler
	public void onEntityHit(EntityDamageByEntityEvent e) {

		if (GameStages.isState(GameStages.LOBBY)
				|| GameStages.isState(GameStages.Countdown)
				|| GameStages.isState(GameStages.PostGame)) {
			e.setCancelled(true);
			return;
		}

		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if (spawnProt.contains(p)) {
				e.setCancelled(true);
				return;
			}
		} else {
			return;
		}

		if (e.getDamager() instanceof Snowball) {

			Snowball s = (Snowball) e.getDamager();
			Player dead = (Player) e.getEntity();
			Player p = (Player) s.getShooter();

			if (Team.getTeam(p).equals(Team.getTeam(dead))) {
				e.setCancelled(true);
				return;
			}
			for (Guns a : Guns.getGuns()) {

				if (a.getMaterial() == p.getItemInHand().getType()) {
					e.setDamage(a.getPower());
				}
			}

		}
		if (e.getDamager() instanceof Player) {

			Player p = (Player) e.getDamager();
			Player dead = (Player) e.getEntity();

			if (Team.getTeam(p).equals(Team.getTeam(dead))) {
				e.setCancelled(true);
				return;
			}

			if (p.getItemInHand().getType() == Material.DIAMOND_SWORD) {
				e.setDamage(15.0);

			}

		}

		if (!(e.getEntity() instanceof Player))
			return;

	}

	@EventHandler
	public void EntityDeath(PlayerDeathEvent e) {

		if (e.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent) {
			EntityDamageByEntityEvent ex = (EntityDamageByEntityEvent) e
					.getEntity().getLastDamageCause();
			if (ex.getDamager() instanceof Snowball
					&& ex.getEntity() instanceof Player) {

				Player killed = (Player) ex.getEntity();
				Snowball s = (Snowball) ex.getDamager();
				Player p = (Player) s.getShooter();

				if (Team.kills.get(p) == null || Team.kills == null) {
					Team.kills.put(p, 1);
					e.setDeathMessage(ChatColor.BLUE + "[Mineshot] "
							+ ChatColor.GOLD + p.getName() + " has killed "
							+ killed.getName() + "!");
					p.sendMessage(ChatColor.GOLD + "You killed "
							+ killed.getName() + "!(+5 Coins!)");
					Team.setLives(Team.getTeam(killed),
							Team.getLives(Team.getTeam(killed)) - 1);
				} else {
					Team.kills.put(p, Team.kills.get(p) + 1);
					e.setDeathMessage(ChatColor.BLUE + "[Mineshot] "
							+ ChatColor.GOLD + p.getName() + " has killed "
							+ killed.getName() + "!");
					p.sendMessage(ChatColor.GOLD + "You killed "
							+ killed.getName() + "!(+5 Coins!)");
					Team.setLives(Team.getTeam(killed),
							Team.getLives(Team.getTeam(killed)) - 1);

					if (Team.getLives(Team.getTeam(killed)) == 0) {
						GameStart.stop(Team.getTeam(p));
					}
				}

				if (Team.deaths.get(killed) == null || Team.deaths == null) {
					Team.deaths.put(killed, 1);
				} else {
					Team.deaths.put(killed, Team.deaths.get(killed) + 1);
				}

				if (Team.getTeam(p) == Team.getTeam("Red")) {
					p.setPlayerListName(ChatColor.RED + p.getName()
							+ ChatColor.GRAY + "    [" + Team.kills.get(p)
							+ "/" + Team.deaths.get(p) + "]");
				} else if (Team.getTeam(p) == Team.getTeam("Blue")) {
					p.setPlayerListName(ChatColor.BLUE + p.getName()
							+ ChatColor.GRAY + "    [" + Team.kills.get(p)
							+ "/" + Team.deaths.get(p) + "]");
				}
				if (Team.getTeam(killed) == Team.getTeam("Red")) {
					killed.setPlayerListName(ChatColor.RED + killed.getName()
							+ ChatColor.GRAY + "    [" + Team.kills.get(killed)
							+ "/" + Team.deaths.get(killed) + "]");
				} else if (Team.getTeam(killed) == Team.getTeam("Blue")) {
					killed.setPlayerListName(ChatColor.BLUE + killed.getName()
							+ ChatColor.GRAY + "    [" + Team.kills.get(killed)
							+ "/" + Team.deaths.get(killed) + "]");
				}
			}

			if (ex.getDamager() instanceof Player
					&& ex.getEntity() instanceof Player) {

				Player p = (Player) ex.getDamager();
				Player killed = (Player) ex.getEntity();

				if (Team.kills.get(p) == null || Team.kills == null) {
					Team.kills.put(p, 1);
					e.setDeathMessage(ChatColor.BLUE + "[Mineshot] "
							+ ChatColor.GOLD + p.getName() + " has killed "
							+ killed.getName() + "!");
					p.sendMessage(ChatColor.GOLD + "You killed "
							+ killed.getName() + "!(+5 Coins!)");
					Team.setLives(Team.getTeam(killed),
							Team.getLives(Team.getTeam(killed)) - 1);
				} else {
					Team.kills.put(p, Team.kills.get(p) + 1);
					e.setDeathMessage(ChatColor.BLUE + "[Mineshot] "
							+ ChatColor.GOLD + p.getName() + " has killed "
							+ killed.getName() + "!");
					p.sendMessage(ChatColor.GOLD + "You killed "
							+ killed.getName() + "!(+5 Coins!)");
					Team.setLives(Team.getTeam(killed),
							Team.getLives(Team.getTeam(killed)) - 1);

					if (Team.getLives(Team.getTeam(killed)) == 0) {
						GameStart.stop(Team.getTeam(p));
					}
				}

				if (Team.deaths.get(killed) == null || Team.deaths == null) {
					Team.deaths.put(killed, 1);
				} else {
					Team.deaths.put(killed, Team.deaths.get(killed) + 1);
				}

				if (Team.getTeam(p) == Team.getTeam("Red")) {
					p.setPlayerListName(ChatColor.RED + p.getName()
							+ ChatColor.GRAY + "    [" + Team.kills.get(p)
							+ "/" + Team.deaths.get(p) + "]");
				} else if (Team.getTeam(p) == Team.getTeam("Blue")) {
					p.setPlayerListName(ChatColor.BLUE + p.getName()
							+ ChatColor.GRAY + "    [" + Team.kills.get(p)
							+ "/" + Team.deaths.get(p) + "]");
				}
				if (Team.getTeam(killed) == Team.getTeam("Red")) {
					killed.setPlayerListName(ChatColor.RED + killed.getName()
							+ ChatColor.GRAY + "    [" + Team.kills.get(killed)
							+ "/" + Team.deaths.get(killed) + "]");
				} else if (Team.getTeam(killed) == Team.getTeam("Blue")) {
					killed.setPlayerListName(ChatColor.BLUE + killed.getName()
							+ ChatColor.GRAY + "    [" + Team.kills.get(killed)
							+ "/" + Team.deaths.get(killed) + "]");
				}

			}
		}

		spawnProt.add(e.getEntity());

		final Player p = e.getEntity();
		Bukkit.getServer().getScheduler()
				.scheduleSyncDelayedTask(Main.instance, new Runnable() {

					@Override
					public void run() {

						spawnProt.remove(p);

					}
				}, 20 * 5);

	}

	@EventHandler
	public void onPlayerDeath(final PlayerDeathEvent e) {

		new BukkitRunnable() {
			@Override
			public void run() {
				try {
					Object nmsPlayer = e.getEntity().getClass()
							.getMethod("getHandle").invoke(e.getEntity());
					Object con = nmsPlayer.getClass()
							.getDeclaredField("playerConnection")
							.get(nmsPlayer);

					Class<?> EntityPlayer = Class.forName(nmsPlayer.getClass()
							.getPackage().getName()
							+ ".EntityPlayer");

					Field minecraftServer = con.getClass().getDeclaredField(
							"minecraftServer");
					minecraftServer.setAccessible(true);
					Object mcserver = minecraftServer.get(con);

					Object playerlist = mcserver.getClass()
							.getDeclaredMethod("getPlayerList")
							.invoke(mcserver);
					Method moveToWorld = playerlist.getClass().getMethod(
							"moveToWorld", EntityPlayer, int.class,
							boolean.class);
					moveToWorld.invoke(playerlist, nmsPlayer, 0, false);

					Player killed = e.getEntity();

					if (Team.getTeam(killed) == Team.getTeam("Red")) {
						killed.teleport(LocationUtils.RedSpawn);
					} else {
						killed.teleport(LocationUtils.BlueSpawn);
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}.runTaskLater(Main.instance, 2);

		e.getDrops().clear();
	}

	@EventHandler
	public void respawn(PlayerRespawnEvent e) {

		Shoot s = null;
		for (Shoot sh : ShootEvent.playerShoot) {

			if (sh.getPlayer() == e.getPlayer()) {
				s = sh;
			}

		}

		for (Guns g : Guns.getGuns()) {

			if (e.getPlayer().getItemInHand().getType() == g.getMaterial()) {

				s.reloadGun(g);

			}

		}

	}

	@EventHandler
	public void playerDamage(EntityDamageEvent e) {
		if (e.getCause() == DamageCause.FALL) {
			e.setCancelled(true);
		}
	}

	public static void addCoins() {

		for (Player p : Team.kills.keySet()) {

			if (Team.kills.get(p) == null) {
				return;
			}

			int kills = Team.kills.get(p);
			int coins = 5 * kills;

			Main.openConnection();
			try {
				if (Main.playerDataContainsPlayer(p)) {

					if (Main.connection == null) {
						Main.openConnection();
					}
					int i = 0;

					PreparedStatement sql = Main.connection
							.prepareStatement("SELECT coins FROM `player_data` WHERE uuid=?;");
					sql.setString(1, "" + p.getUniqueId());

					ResultSet result = sql.executeQuery();
					result.next();

					i = result.getInt("coins");

					PreparedStatement coinsUpdate = Main.connection
							.prepareStatement("UPDATE `player_data` SET coins=? WHERE uuid=?;");
					coinsUpdate.setInt(1, i + coins);
					coinsUpdate.setString(2, "" + p.getUniqueId());
					coinsUpdate.executeUpdate();

					coinsUpdate.close();
					sql.close();
					result.close();
				} else {
					if (Main.connection == null) {
						Main.openConnection();
					}
					PreparedStatement newPlayer = Main.connection
							.prepareStatement("INSERT INTO `player_data` values(\""
									+ p.getUniqueId() + "\", coins)");
					newPlayer.execute();
					newPlayer.close();
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			} finally {
				Main.closeConnection();
			}
		}

	}

	@EventHandler
	public void EntitySpawn(EntitySpawnEvent e) {

		if (e.getEntity() instanceof Player) {
			return;
		}

		e.setCancelled(true);

	}
}
