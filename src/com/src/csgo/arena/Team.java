package com.src.csgo.arena;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;

public class Team {

	private static List<Team> allTeams = new ArrayList<Team>();
	private static HashMap<Player, Team> team = new HashMap<Player, Team>();
	public static HashMap<Team, Integer> Lives = new HashMap<Team, Integer>();
	public static HashMap<Player, Integer> kills = new HashMap<Player, Integer>();
	public static HashMap<Player, Integer> deaths = new HashMap<Player, Integer>();
	private ArrayList<Player> players = new ArrayList<Player>();

	private String TeamName;

	public Team(String TeamName) {

		this.TeamName = TeamName;
		allTeams.add(this);

	}

	public String getName() {
		return this.TeamName;
	}

	public void add(Player p) {
		team.put(p, this);
		players.add(p);
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public boolean remove(Player p) {
		if (!(hasTeam(p))) {
			return false;
		} else {
			team.remove(p);
			return true;
		}
	}

	public static boolean hasTeam(Player p) {
		return team.containsKey(p);
	}

	public static Team getTeam(Player p) {
		if (hasTeam(p)) {
			return team.get(p);
		} else {
			return null;
		}
	}

	public static Team getTeam(String name) {
		for (Team t : getTeams())
			if (t.TeamName.equalsIgnoreCase(name))
				return t;

		return null;

	}

	public static List<Team> getTeams() {
		return allTeams;
	}

	public static void setLives(Team team, int i) {
		Lives.put(team, i);
	}

	public static Integer getLives(Team team) {
		return Lives.get(team);
	}

}
