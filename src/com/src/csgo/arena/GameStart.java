package com.src.csgo.arena;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.src.csgo.Guns;
import com.src.csgo.Main;
import com.src.csgo.locationUtils.LocationUtils;
import com.src.csgo.scoreboards.InGameScore;
import com.src.csgo.scoreboards.PreGame;

public class GameStart implements Listener {

	public static HashMap<Player, ArrayList<Guns>> unlockedGuns = new HashMap<Player, ArrayList<Guns>>();

	public static int j = 0;
	public static int count = 0;
	public static int pregame = 0;
	public static int ingame = 0;
	public static int gameTimer = 0;
	public static HashMap<Player, Guns> playerGun = new HashMap<Player, Guns>();

	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent e) {

		Main.injector.addPlayer(e.getPlayer());

		Bukkit.getServer().getScheduler()
				.scheduleSyncDelayedTask(Main.instance, new Runnable() {

					@Override
					public void run() {

						e.getPlayer().teleport(LocationUtils.Lobby);

					}
				}, 5);

		e.getPlayer().setFoodLevel(20);
		e.getPlayer().setExhaustion(20.0f);

		PreGame.makeScoreboard(e.getPlayer());

		doPregameInv(e.getPlayer());

		if (GameStages.isState(GameStages.LOBBY)) {
			if (Bukkit.getServer().getOnlinePlayers().size() > 1) {
				getGuns(e.getPlayer());

				GameStages.setState(GameStages.Countdown);

				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
						"pex reload");

				if (count == 0) {

					count = Bukkit.getScheduler().scheduleSyncRepeatingTask(
							Main.instance, new Runnable() {

								int countdown = 30;

								@Override
								public void run() {

									GameStart.j = countdown;

									if (countdown != 0) {
										for (Player p : Bukkit
												.getOnlinePlayers()) {
											p.sendMessage(ChatColor.GOLD
													+ "Game starts in "
													+ ChatColor.RED + countdown
													+ ChatColor.GOLD
													+ " seconds!");
										}
									}

									if (countdown == 10) {
										new Team("Red");
										new Team("Blue");
									}

									if (countdown == 0) {
										for (Player p : Bukkit
												.getOnlinePlayers()) {
											Main.getCoins(p);
										}
										Bukkit.getServer().getScheduler()
												.cancelTask(pregame);
										doIngameSchedule();
										start();

									}

									countdown--;

								}
							}, 20, 20);

				}
			}

			Bukkit.getServer().getScheduler()
					.scheduleSyncDelayedTask(Main.instance, new Runnable() {

						@Override
						public void run() {
							earlyStop();

						}
					}, 100);
		}

	}

	public static void start() {

		Bukkit.getServer().getScheduler().cancelTask(count);

		GameStages.setState(GameStages.INGAME);

		int i = 0;
		for (Player p : Bukkit.getServer().getOnlinePlayers()) {

			p.getInventory().clear();
			p.updateInventory();

			if (!(playerGun.containsKey(p)) || playerGun == null) {
				p.getInventory().setItem(0,
						new ItemStack(Guns.M4.getMaterial()));
			} else {
				ItemStack gun = new ItemStack(playerGun.get(p).getMaterial());
				ItemMeta gMeta = gun.getItemMeta();

				gMeta.setDisplayName("" + playerGun.get(p));
				gun.setItemMeta(gMeta);

				if (p.hasPermission("Mineshot.Special")) {
					gun = Main.addGlow(gun);
				}

				p.getInventory().setItem(0, gun);
			}
			ItemStack knife = new ItemStack(Material.DIAMOND_SWORD);
			ItemMeta kMeta = knife.getItemMeta();

			kMeta.setDisplayName("Knife");
			knife.setItemMeta(kMeta);
			p.getInventory().setItem(1, knife);

			if (i >= Team.getTeams().size()) {
				i = 0;
			}

			Team.getTeam(Team.getTeams().get(i++).getName()).add(p);
			if (Team.getTeam(p) == Team.getTeam("Red")) {
				p.teleport(LocationUtils.RedSpawn);
				p.setPlayerListName(ChatColor.RED + p.getName()
						+ ChatColor.GRAY + "    [0/0]");
			} else if (Team.getTeam(p) == Team.getTeam("Blue")) {
				p.teleport(LocationUtils.BlueSpawn);
				p.setPlayerListName(ChatColor.BLUE + p.getName()
						+ ChatColor.GRAY + "    [0/0]");
			}
			p.sendMessage(ChatColor.GREEN + "You have are on the "
					+ Team.getTeam(p).getName() + " team!");
			Team.kills.put(p, 0);
			Team.deaths.put(p, 0);

			InvisCatch(p);
		}
		for (Team t : Team.getTeams()) {
			Team.setLives(t, 25);
		}

	}

	public static void InvisCatch(Player player) {
		for (Player p : Bukkit.getOnlinePlayers()) {

			p.showPlayer(player);
			player.showPlayer(p);

		}

	}

	public static void getGuns(Player p) {

		ArrayList<Guns> unlocks = new ArrayList<Guns>();

		Main.openGunsConnection();
		try {
			if (Main.gunsContainsPlayer(p)) {

				if (Main.connectionGuns == null) {
					Main.openGunsConnection();
				}

				PreparedStatement sql = Main.connectionGuns
						.prepareStatement("SELECT G36C FROM `unlockedGuns` WHERE uuid=?;");
				sql.setString(1, "" + p.getUniqueId());

				ResultSet result = sql.executeQuery();
				result.next();

				boolean bool1 = result.getBoolean("G36C");

				PreparedStatement sql1 = Main.connectionGuns
						.prepareStatement("SELECT M4 FROM `unlockedGuns` WHERE uuid=?;");
				sql1.setString(1, "" + p.getUniqueId());

				ResultSet result1 = sql1.executeQuery();
				result1.next();

				boolean bool2 = result1.getBoolean("M4");

				PreparedStatement sql2 = Main.connectionGuns
						.prepareStatement("SELECT Scar FROM `unlockedGuns` WHERE uuid=?;");
				sql2.setString(1, "" + p.getUniqueId());

				ResultSet result2 = sql2.executeQuery();
				result2.next();

				boolean bool3 = result2.getBoolean("Scar");

				PreparedStatement sql3 = Main.connectionGuns
						.prepareStatement("SELECT Barrett FROM `unlockedGuns` WHERE uuid=?;");
				sql3.setString(1, "" + p.getUniqueId());

				ResultSet result3 = sql3.executeQuery();
				result3.next();
				boolean bool4 = result3.getBoolean("Barrett");

				if (bool1 == true) {
					unlocks.add(Guns.G36C);
				}
				if (bool2 == true) {
					unlocks.add(Guns.M4);
				}
				if (bool3 == true) {
					unlocks.add(Guns.Scar);
				}
				if (bool4 == true) {
					unlocks.add(Guns.Barrett);
				}

				unlockedGuns.put(p, unlocks);

				sql.close();
				sql1.close();
				sql2.close();
				sql3.close();
				result.close();
			} else {
				if (Main.connectionGuns == null) {
					Main.openGunsConnection();
				}
				PreparedStatement newPlayer = Main.connectionGuns
						.prepareStatement("INSERT INTO `unlockedGuns` values(\""
								+ p.getUniqueId()
								+ "\", false, true, false, false)");
				newPlayer.execute();
				newPlayer.close();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			Main.closeGunsConnection();
		}

	}

	public static void doPregameInv(Player p) {

		p.getInventory().clear();

		ItemStack itemstack = new ItemStack(Material.COOKED_BEEF);
		ItemMeta itemMeta = itemstack.getItemMeta();

		itemMeta.setDisplayName(ChatColor.GOLD + "Loadout selector!");
		itemstack.setItemMeta(itemMeta);

		p.getInventory().setItem(0, itemstack);

	}

	public static void doPregameSchedule() {
		pregame = Bukkit.getServer().getScheduler()
				.scheduleSyncRepeatingTask(Main.instance, new Runnable() {

					@Override
					public void run() {
						for (Player p : Bukkit.getOnlinePlayers()) {
							PreGame.updateScoreboard(p);
						}

					}
				}, 5, 5);

	}

	public static void doIngameSchedule() {

		ingame = Bukkit.getServer().getScheduler()
				.scheduleSyncRepeatingTask(Main.instance, new Runnable() {

					@Override
					public void run() {
						for (Player p : Bukkit.getOnlinePlayers()) {
							InGameScore.updateScoreboard(p);
						}

					}
				}, 20, 20);

		gameTimer = Bukkit.getServer().getScheduler()
				.scheduleSyncRepeatingTask(Main.instance, new Runnable() {

					int time = 20 * 60 * 15;
					int interval = 20 * 60;

					@Override
					public void run() {

						if (time % interval == 0) {
							for (Player all : Bukkit.getOnlinePlayers()) {
								all.sendMessage(ChatColor.BLUE
										+ "[MineShot] > There are " + time
										/ interval
										+ " minutes left in the game!");
							}
						}

						time--;

					}
				}, 20, 20);
	}

	public static void stop(Team winner) {

		GameStages.setState(GameStages.PostGame);

		Bukkit.getServer().getScheduler().cancelTask(ingame);

		for (Player pl : Bukkit.getOnlinePlayers()) {
			pl.sendMessage(ChatColor.BOLD + winner.getName()
					+ " has won the game!");
			pl.sendMessage(ChatColor.RED
					+ "You will be sent back to the hub in 5 seconds!");

			int killcoins = Team.kills.get(pl);
			killcoins = killcoins * 5;

			if (winner == Team.getTeam(pl)) {
				pl.sendMessage(ChatColor.GOLD
						+ "You have earned 20 coins for winning!");
				killcoins = killcoins + 20;
			}

			int i = killcoins + Main.coins.get(pl);

			sendCoins(pl, i);
		}

		Bukkit.getServer().getScheduler()
				.scheduleSyncDelayedTask(Main.instance, new Runnable() {

					@Override
					public void run() {

						ByteArrayDataOutput out = ByteStreams.newDataOutput();

						out.writeUTF("Connect");
						out.writeUTF("Hub");

						for (Player pl : Bukkit.getOnlinePlayers()) {
							pl.sendPluginMessage(Main.instance, "BungeeCord",
									out.toByteArray());
						}
					}
				}, 20 * 5);

		Bukkit.getServer().getScheduler()
				.scheduleSyncDelayedTask(Main.instance, new Runnable() {

					@Override
					public void run() {
						Bukkit.getServer().shutdown();
					}
				}, 20 * 10);
	}

	public static void earlyStop() {

		GameStages.setState(GameStages.PostGame);

		Bukkit.getServer().getScheduler()
				.scheduleSyncDelayedTask(Main.instance, new Runnable() {

					@Override
					public void run() {
						if (Bukkit.getOnlinePlayers().size() <= 1) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(ChatColor.RED
										+ "There are not enough players! Sending you back to the hub!");

								ByteArrayDataOutput out = ByteStreams
										.newDataOutput();

								out.writeUTF("Connect");
								out.writeUTF("Hub");

								p.sendPluginMessage(Main.instance,
										"BungeeCord", out.toByteArray());
							}
						}
					}
				}, 100);

		Bukkit.getServer().getScheduler()
				.scheduleSyncDelayedTask(Main.instance, new Runnable() {

					@Override
					public void run() {
						if (Bukkit.getOnlinePlayers().size() <= 1) {
							Bukkit.getServer().shutdown();
						}
					}
				}, 200);

	}

	public synchronized static void sendCoins(Player p, int i) {

		try {
			if (Main.playerDataContainsPlayer(p)) {

				Main.openConnection();

				PreparedStatement sql = Main.connection
						.prepareStatement("UPDATE `player_data` SET coins=? WHERE uuid=?;");
				sql.setInt(1, i);
				sql.setString(2, "" + p.getUniqueId());
				sql.execute();
				sql.close();
			} else {
				if (Main.connection == null) {
					Main.openConnection();
				}
				PreparedStatement newPlayer = Main.connection
						.prepareStatement("INSERT INTO `player_data` values(\""
								+ p.getUniqueId() + "\", 0)");
				newPlayer.execute();
				newPlayer.close();
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			Main.closeConnection();
		}

	}

	@EventHandler
	public void pq(PlayerQuitEvent e) {

		e.setQuitMessage(ChatColor.BLUE + "[Mineshot] " + ChatColor.GOLD
				+ e.getPlayer().getName() + " has left the game!");

		if (GameStages.isState(GameStages.LOBBY)
				|| GameStages.isState(GameStages.Countdown)) {
			if (Bukkit.getOnlinePlayers().size() <= 1) {
				earlyStop();
			}
		}

		if (GameStages.isState(GameStages.INGAME)) {
			for (Team t : Team.getTeams()) {
				if (t.getPlayers().size() == 0) {
					if (t.getName().equalsIgnoreCase("Blue")) {
						stop(Team.getTeam("Red"));
					} else {
						stop(Team.getTeam("Blue"));
					}
				}
			}

		}
	}

	@EventHandler
	public void food(FoodLevelChangeEvent e) {
		e.setCancelled(true);
	}
}
