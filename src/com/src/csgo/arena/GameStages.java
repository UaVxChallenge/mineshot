package com.src.csgo.arena;

public enum GameStages {

	LOBBY(true), Countdown(false), INGAME(false), PostGame(false);

	private boolean canJoin;

	private static GameStages gameStage;

	GameStages(boolean canJoin) {

		this.canJoin = canJoin;

	}

	public boolean canJoin() {
		return this.canJoin;
	}

	public static void setState(GameStages stage) {

		GameStages.gameStage = stage;

	}

	public static boolean isState(GameStages gameStage) {

		return GameStages.gameStage == gameStage;

	}

	public static GameStages getState() {
		return gameStage;
	}

}
